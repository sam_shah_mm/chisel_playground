#include <iostream>
#include <fstream>
#include <cmath>

#include "VTop.h"

#include "TestBench.hpp"

#define RUN_TEST(test_bench, test_name)                     \
	{                                                       \
		printf("Running %s...\n", #test_name);              \
		printf("Got result %d\n\n", test_name(test_bench)); \
	}

bool cmp_float(double truev, double computed, double atol = 1e-3, double rtol = 1e-3)
{
	return fabs(truev - computed) < (rtol * fabs(truev) + atol);
}

int test0(TestBench<VTop> &tb)
{
	VTop* top = tb.GetTop();
	tb.Poke(top->io_x, 0);

	return top->io_x_n;
}

int main(int argc, const char **argv)
{
	Verilated::commandArgs(argc, argv);

	TestBench<VTop> tb_top("top");
	RUN_TEST(tb_top, test0);

	return 0;
}
