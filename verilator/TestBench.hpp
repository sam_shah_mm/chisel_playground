#pragma once

#include <cstdio>
#include <cstdlib>
#include <cstdint>

#include <verilated.h>
#if VM_TRACE
#include <verilated_vcd_c.h> // Trace file format header
#endif

// test helpers...
//#define FLOAT_TO_FIXED(fl) ((((uint32_t)fl) << 16) + (uint32_t)((fl - (uint32_t)fl) * 65536))
//#define FIXED_TO_FLOAT(fix) ((double)(fix >> 16) + ((double)(fix & 0xFFFF) / 65536.0))
#define BP_COUNT (16)
#define FLOAT_TO_FIXED(fl) ( (int32_t)(fl * (1 << BP_COUNT)) )
#define FIXED_TO_FLOAT(fix) ( (double)((int32_t)fix) / (double)(1 << BP_COUNT) )
#define BIT_REVERSE_BYTE(b) ((uint8_t)((((uint8_t)b * 0x80200802ULL) & 0x0884422110ULL) * 0x0101010101ULL >> 32))

template <class T>
class TestBench
{
public:
    TestBench(const char* name, const char* half_clk_period="10ns")
    {
        this->top = new T(name);
        clean_name = top->name();
#if VM_TRACE                          // If verilator was invoked with --trace
        tfp->set_time_unit("1ps");
        tfp->set_time_resolution(half_clk_period);
        Verilated::traceEverOn(true); // Verilator must compute traced signals

        VL_PRINTF("Enabling waves @ %s...\n", clean_name.c_str());
        top->trace(tfp, 99);            // Trace 99 levels of hierarchy

        Verilated::mkdir("logs");
        std::string tfp_fname = "logs/" + clean_name + "_dump.vcd";
        tfp->open(tfp_fname.c_str()); // Open the dump file
#endif
    }

    T *GetTop()
    {
        return top;
    }

    // elapsed half-cycles
    uint64_t GetTs()
    {
        return ts;
    }

    template <class T1, class T2>
    void Poke(T1 &input, T2 val)
    {
        input = (T2)val; // force it in
        top->eval();
    }

    void ClockLow()
    {
        top->clock = 0;
        top->eval();
#if VM_TRACE
        if (tfp)
            tfp->dump(ts++); // Create waveform trace for this timestamp
#endif
    }

    void ClockHigh()
    {
        top->clock = 1;
        top->eval();
#if VM_TRACE
        if (tfp)
            tfp->dump(ts++); // Create waveform trace for this timestamp
#endif
    }

    void ClockStep()
    {
        ClockLow();
        ClockHigh();
    }

    ~TestBench()
    {
#if VM_TRACE
        if (tfp)
            tfp->close();
#endif
#if VM_COVERAGE
        Verilated::mkdir("logs");
        std::string cov_fname = "logs/" + clean_name + "_cov.dat";
        VerilatedCov::write(cov_fname.c_str());
#endif
        delete top;
    }

private:
    VerilatedVcdC *tfp = new VerilatedVcdC;
    uint64_t ts = 0;
    T *top;
    std::string clean_name;
};