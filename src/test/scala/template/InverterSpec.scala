package ss.chisel.template

import org.scalatest._
import chiseltest._
import chisel3._

class InverterSpec extends FlatSpec with ChiselScalatestTester with Matchers {
  behavior of "Inverter"
  it should "invert its one input" in {
    test(new Inverter) { c =>
    	c.io.x.poke(0.B)
	c.io.x_n.expect(1.B)
	    
	c.io.x.poke(1.B)
	c.io.x_n.expect(0.B)
    }
  }
}
