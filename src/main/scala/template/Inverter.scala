package ss.chisel.template

import chisel3._
import chisel3.util._

class Inverter extends Module {
    val io = IO(new Bundle {
        val x = Input(Bool())
        val x_n = Output(Bool())
    })	  
    
    io.x_n := !io.x
}
