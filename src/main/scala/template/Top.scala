package ss.chisel.template

import chisel3._
import chisel3.util._
import chisel3.stage.ChiselStage


class Top extends Inverter
 
object TopApp extends App {
    (new ChiselStage).emitVerilog(new Top())
}
