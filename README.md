Chisel Playground
=================

# How to use
Try following this README with the included inverter module first.

Follow the directory structure to add your own Chisel modules. You can write ScalaTest classes to do some basic testing of modules with a ChiselScalatestTester. To run your test, use `sbt "testOnly <test class name>"` (for example, `sbt "testOnly ss.chisel.template.InverterSpec"`).

Select which ever module you want to be at the top-level in the Verilog output using the file `Top.scala` and changing the class `Top` to extend that module. Run `sbt run` from the root directory to emit Verilog code (this will go into `Top.v`).

To create a Verilator testbench, edit `sim_main.cpp` and then run `make` to run the test bench. You can also run `make trace` to see the output .vcd file if you have `gtkwave` installed.


# Directory structure
Your Scala/Chisel code should go in `src/main/scala`, while any ScalaTest code should be in `src/test/scala`.

Verilator code is in the `verilator` folder.

